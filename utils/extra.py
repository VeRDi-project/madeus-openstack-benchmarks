import os
import yaml
import socket

import logging
from pathlib import Path
from netaddr import IPRange
from subprocess import (run, check_call)

import enoslib.api as api
from .constants import (MAD_PATH, ANSIBLE_DIR, NEUTRON_EXTERNAL_INTERFACE,
                        FAKE_NEUTRON_EXTERNAL_INTERFACE, NETWORK_INTERFACE,
                        API_INTERFACE, VENV_KOLLA, SYMLINK_NAME)

# These roles are mandatory for the
# the original inventory to be valid
# Note that they may be empty
# e.g. if cinder isn't installed storage may be a empty group
# in the inventory
KOLLA_MANDATORY_GROUPS = [
    "control",
    "compute",
    "network",
    "storage"
]



def bootstrap_kolla(env):
    """Setups all necessities for calling kolla-ansible.

    - On the local host
      + Patches kolla+ansible sources (if any).
      + Builds globals.yml into result dir.
      + Builds password.yml into result dir.
      + Builds admin+openrc into result dir.
    - On all the hosts
      + Remove the ip addresses on the
        neutron_external_interface (set in the
        inventory hostvars)

    """
    # Write the globals.yml file in the result dir.
    #
    # FIXME: Find a neat way to put this into the next bootsrap_kolla
    # playbook. Then, remove this util function and call directly the
    # playbook from `enos os`.
    globals_path = os.path.join(env['resultdir'], 'globals.yml')
    globals_values = get_kolla_required_values(env)
    globals_values.update(env['config']['kolla'])
    globals_values.update(cwd=env['cwd'])
    with open(globals_path, 'w') as f:
        yaml.dump(globals_values, f, default_flow_style=False)

    # Patch kolla-ansible sources + Write admin-openrc and
    # password.yml in the result dir
    enos_values = mk_enos_values(env)
    playbook = os.path.join(ANSIBLE_DIR, 'bootstrap_kolla.yml')

    api.run_ansible([playbook], env['inventory'], extra_vars=enos_values)


def get_kolla_required_values(env):
    """Returns a dictionary with all values required by kolla-ansible
    based on the Enos environment.

    """
    values = {
        'kolla_internal_vip_address': env['config']['vip'],
        'influx_vip':                 env['config']['influx_vip'],
        'kolla_ref':                  env['config']['kolla_ref'],
        'resultdir':                  env['resultdir']
    }

    #values['openstack_release'] = OPENSTACK_RELEASE

    return values


def mk_kolla_values(src_path, required_values, user_values):
    """Builds a dictionary with all kolla values.

    :param src_path: Path to kolla-ansible sources.

    :param required_values: Values required by kolla-ansible.

    :param user_values: User specic kolla values as defined into
        the reservation file.

    return values related to kolla-ansible
    """
    kolla_values = {}

    # Get kolla-ansible `all.yml` values
    with open(os.path.join(
            src_path, 'ansible', 'group_vars', 'all.yml'), 'r') as f:
        kolla_values.update(yaml.load(f))

    # Override with required values
    kolla_values.update(required_values)

    # Override with user specific values
    kolla_values.update(user_values)

    return kolla_values


def mk_enos_values(env):
    "Builds a dictionary with all enos values based on the environment."
    enos_values = {}

    # Get all kolla values
    enos_values.update(mk_kolla_values(
        os.path.join(env['resultdir'], 'kolla'),
        get_kolla_required_values(env),
        env['config']['kolla']))

    # Update with user specific values (except already got kolla)
    enos_values.update(
        {k: v for k, v in env['config'].items() if k != "kolla"})

    # Add the Current Working Directory (cwd)
    enos_values.update(cwd=env['cwd'])

    # Defer the following variables to the environment
    # These two interfaces are set in the host vars
    # We don't need them here since they will overwrite those in the inventory
    enos_values.pop(NEUTRON_EXTERNAL_INTERFACE, None)
    enos_values.pop(NETWORK_INTERFACE, None)

    return enos_values


def generate_inventory(roles, networks, base_inventory, dest):
    """
    Generate the inventory.
    It will generate a group for each role in roles and
    concatenate them with the base_inventory file.
    The generated inventory is written in dest
    """
    fake_interfaces = []
    fake_networks = []
    provider_net = lookup_network(networks, [NEUTRON_EXTERNAL_INTERFACE])
    if not provider_net:
        logging.error("The %s network is missing" % NEUTRON_EXTERNAL_INTERFACE)
        logging.error("EnOS will try to fix that ....")
        fake_interfaces = [FAKE_NEUTRON_EXTERNAL_INTERFACE]
        fake_networks = [NEUTRON_EXTERNAL_INTERFACE]

    api.generate_inventory(
        roles,
        networks,
        dest,
        check_networks=True,
        fake_interfaces=fake_interfaces,
        fake_networks=fake_networks
    )

    with open(dest, 'a') as f:
        f.write("\n")
        # generate mandatory groups that are empty
        mandatory = [group for group in KOLLA_MANDATORY_GROUPS
                       if group not in roles.keys()]
        for group in mandatory:
            f.write("[%s]\n" % group)

        with open(base_inventory, 'r') as a:
            for line in a:
                f.write(line)

    logging.info("Inventory file written to " + dest)


def lookup_network(networks, roles):
    """Lookup a network by its roles (in order).
    We assume that one role can't be found in two different networks
    """
    for role in roles:
        for network in networks:
            if role in network["roles"]:
                return network
    return None


def get_vip_pool(networks):
    """Get the provider net where vip can be taken.
    In kolla-ansible this is the network with the api_interface role.
    In kolla-ansible api_interface defaults to network_interface.
    """
    provider_net = lookup_network(networks, [API_INTERFACE, NETWORK_INTERFACE])
    if provider_net:
        return provider_net

    msg = "You must declare %s" % " or ".join(
        [API_INTERFACE, NETWORK_INTERFACE])
    raise Exception(msg)


def pop_ip(provider_net):
    """Picks an ip from the provider_net
    It will first take ips in the extra_ips if possible.
    extra_ips is a list of isolated ips whereas ips described
    by the [provider_net.start, provider.end] range is a continuous
    list of ips.
    """
    # Construct the pool of ips
    extra_ips = provider_net.get('extra_ips', [])
    if len(extra_ips) > 0:
        ip = extra_ips.pop()
        provider_net['extra_ips'] = extra_ips
        return ip

    ips = list(IPRange(provider_net['start'],
                       provider_net['end']))

    # Get the next ip
    ip = str(ips.pop())

    # Remove this ip from the env
    provider_net['end'] = str(ips.pop())

    return ip


def seekpath(path):
    """Seek for an enos file `path` and returns its absolute counterpart.

    Seeking rules are:
    - If `path` is absolute then return it
    - Otherwise, look for `path` in the current working directory
    - Otherwise, look for `path` in the source directory
    - Otherwise, raise an `EnosFilePathError` exception

    """
    abspath = None

    if os.path.isabs(path):
        abspath = path
    elif os.path.exists(os.path.abspath(path)):
        abspath = os.path.abspath(path)
    elif os.path.exists(os.path.join(MAD_PATH, path)):
        abspath = os.path.join(MAD_PATH, path)
    else:
        raise EnosFilePathError(
            path,
            "There is no path to %s, neither in current "
            "directory (%s) nor enos sources (%s)."
            % (path, os.getcwd(), MAD_PATH))

    logging.debug("Seeking %s path resolves to %s", path, abspath)

    return abspath


def check_call_in_venv(venv_dir, cmd):
    """Calls command in kolla virtualenv."""
    def check_venv(venv_path):

        if not os.path.exists(venv_path):
            check_call("virtualenv -p python2 %s" % venv_path, shell=True)
            check_call_in_venv(venv_dir, "pip install --upgrade pip")

    cmd_in_venv = []
    cmd_in_venv.append(". %s/bin/activate " % venv_dir)
    cmd_in_venv.append('&&')
    if isinstance(cmd, list):
        cmd_in_venv.extend(cmd)
    else:
        cmd_in_venv.append(cmd)
    check_venv(venv_dir)
    _cmd = ' '.join(cmd_in_venv)
    logging.debug(_cmd)
    return check_call(_cmd, shell=True)


def in_kolla(cmd):
    check_call_in_venv(VENV_KOLLA, cmd)


def get_host_name_and_ip():
    try:
        host_name = socket.gethostname()
        host_ip = socket.gethostbyname(host_name)
        return host_name, host_ip
    except:
        print("Unable to get Hostname and IP")


def run_ansible(playbook, inventory, config_vars=[], tags=None):

    #if not config_vars:
    vars_file = str(Path(ANSIBLE_DIR) / "kolla_group_vars" / "all.yml")
    global_file = str(Path(SYMLINK_NAME) / "globals.yml")
    pass_file = str(Path(SYMLINK_NAME) / "passwords.yml")

    config_vars.append("@" + vars_file)
    config_vars.append("@" + global_file)
    config_vars.append("@" + pass_file)
    #config_vars.append("action=deploy")

    if not playbook or not inventory:
        print("Both playbook and inventory must be provided - leaving... :(")
        sys.exit(1)

    # Forge the desired call to Ansible:
    cmd = ["ansible-playbook"]
    cmd.extend(("-i", inventory))
    for config_var in config_vars:
        cmd.extend(("-e", config_var))
    if tags:
        cmd.extend(("--tags", tags))
    cmd.append(playbook)

    # Call ansible and raise exception if problems occured
    if run(cmd).returncode:
        raise Exception("Something wrong happened during the deployment.")

