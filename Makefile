
CURRENT_DIR := $(shell pwd)
ORCA_DIR := $(CURRENT_DIR)/venv/bin

PACKAGE_TARGETS := ansible ansible.cfg \
	bench-execo.py inventories mad-enoslib.py Makefile \
	README.md requirements.txt reservation* tasks.py utils

CTAGS := $(shell command -v ctags 2> /dev/null)
CTAGS-exists : ; @which ctags > /dev/null
PIP-exists := $(shell command -v pip3 2> /dev/null)

check_pip3:
ifndef PIP-exists
	$(error "pip3 is not available - please install it.")
endif

install_deps: check_pip3 requirements.txt get_orca
	pip3 install -r requirements.txt

get_orca:
	curl -L -0 -o orca-1.2.1-x86_64.AppImage https://github.com/plotly/orca/releases/download/v1.2.1/orca-1.2.1-x86_64.AppImage
	mv orca-1.2.1-x86_64.AppImage $(ORCA_DIR)
	chmod +x $(ORCA_DIR)/orca-1.2.1-x86_64.AppImage
	ln -sf $(ORCA_DIR)/orca-1.2.1-x86_64.AppImage $(ORCA_DIR)/orca
	chmod +x $(ORCA_DIR)/orca

remove_deps:
	-rm -rf venv

package: mad.tgz
mad.tgz: $(PACKAGE_TARGETS)
	tar -zhcvf $@\
		--exclude "current"\
		--exclude ".vagrant"\
		--exclude "enos_20*"\
		--exclude "venv"\
		--exclude "__pycache__"\
		--exclude "*.swo"\
		--exclude "*.swp" --transform 's,^,mad/,' $^

tags: CTAGS-exists
	ctags -R --fields=+l --languages=python --python-kinds=-iv \
	    --exclude=backups --exclude=ansible --exclude=expes

clean:
	-rm -rf enos_*
	-rm -rf MadBench*

mrproper: clean
	-rm -rf current
	-rm -rf venv*
