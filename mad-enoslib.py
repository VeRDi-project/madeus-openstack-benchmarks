# -*- coding: utf-8 -*-

import click
import logging
import yaml

import tasks as t
from datetime import datetime
from utils.constants import ASSEMBLIES
# Necessary to avoid bug in yaml loading in tasks.py l.236
from execo_engine import sweep

logging.basicConfig(level=logging.DEBUG)


@click.group()
def cli():
    pass


@cli.command(help="Claim resources regarding the given configuration file.")
@click.option("-b", "--bootstrap",
        is_flag=True,
        help="Bootstrap concerto-node.")
@click.option("-c", "--conf",
        type=click.Path(exists=True),
        default="reservation.yaml",
        help="Path to the configuration file describing the deployment.")
@click.option("-e", "--env",
        type=click.Path(dir_okay=True),
        help="Path to the environment directory to consider.")
@click.option("-f", "--force",
        is_flag=True,
        help="Destroy and up resources.")
@click.option("-p", "--provider",
        default="g5k",
        type=click.Choice(['vagrant', 'g5k']),
        help="Select the provider (vagrant or g5k).")
@click.option("-r", "--restore",
        type=click.Path(dir_okay=True),
        help="Path to an experiment backup directory to restore.")
@click.option("-t", "--tags",
        help="Only run ansible tasks tagged with these values.")
def deploy(bootstrap, conf, env, force, provider, restore, tags):
    t.deploy(bootstrap, conf, force, provider, restore, tags, env=env)


@cli.command(help="Skip resource reservation and only call enos.yml playbook.")
@click.option("-c", "--conf",
        type=click.Path(exists=True),
        default="reservation.yaml",
        help="Path to the configuration file describing the deployment.")
@click.option("-e", "--env",
        type=click.Path(dir_okay=True),
        help="Path to the environment directory to consider.")
@click.option("-f", "--force",
        is_flag=True,
        help="Destroy and up resources.")
@click.option("-p", "--provider",
        default="g5k",
        type=click.Choice(['vagrant', 'g5k']),
        help="Select the provider (vagrant or g5k).")
@click.option("-r", "--registry",
        is_flag=True,
        help="Deploy an extra node containing a local registry.")
@click.option("-t", "--tags",
        help="Only run ansible tasks tagged with these values.")
def redeploy(conf, env, force, provider, registry, tags):
    t.redeploy(conf, force, provider, registry, tags, env=env)


@cli.command(help="Deploy OpenStack with kolla-ansible.")
@click.option("-e", "--env",
        type=click.Path(dir_okay=True),
        help="Path to the environment directory to consider.")
@click.option("-r", "--reconfigure",
        is_flag=True,
        help="Reconfigure the services after a deployment.")
@click.option("-p", "--pull",
        is_flag=True,
        help="Only preinstall software (e.g. pull docker images).")
@click.option("-t", "--tags",
        help="Only run ansible tasks tagged with these values.")
def install_os(env, pull, reconfigure, tags):
    t.install_os(pull, reconfigure, tags, env=env )


@cli.command(help="Backup the results.")
@click.option("-b", "--backup_dir",
        type=click.Path(dir_okay=True),
        default="backups/" + datetime.today().isoformat(),
        help="Path to the result directory to consider.")
@click.option("-e", "--env",
        type=click.Path(dir_okay=True),
        help="Path to the environment directory to consider.")
def backup(backup_dir, env, **kwargs):
    t.backup(backup_dir, env=env)


@cli.command(help="Generate graphs from the results.")
@click.option("-b", "--backup_dir",
        type=click.Path(dir_okay=True),
        default=None,
        help="Path to the result directory to consider.")
@click.option("-e", "--env",
        type=click.Path(dir_okay=True),
        help="Path to the environment directory to consider.")
@click.option('--exclude', '-x', multiple=True,
        type=click.Choice(ASSEMBLIES),
        help="Exclude an assembly from the analysis (can be used many times).")
def analyze(backup_dir, exclude, env, **kwargs):
    t.analyze(backup_dir, exclude, env=env)


@cli.command(help="Run the benchmarks.")
@click.option("-c", "--conf",
        type=click.Path(exists=True),
        default="reservation.yaml",
        help="Path to the configuration file describing the deployment.")
@click.option("-t", "--test",
        required=True,
        help="Path to the configuration file describing the deployment.")
@click.option("-p", "--provider",
        default="g5k",
        help="Path to the configuration file describing the deployment.")
@click.option("-e", "--env",
        type=click.Path(dir_okay=True),
        help="Path to the environment directory to consider.")
def bench(conf, provider, test, env=None):
    t.bench(conf, test, provider, env=env)


@cli.command(help="Show information of the actual deployment.")
@click.option("-e", "--env",
        type=click.Path(dir_okay=True),
        help="Path to the environment directory to consider.")
@click.option("-o", "--out",
        type=click.Choice(['json', 'pickle', 'yaml']),
        help="Output the result in different formal")
def info(out, env=None):
    t.info(out, env=env)


@cli.command(help="Destroy the deployment and optionally the related resources.")
@click.option("-e", "--env",
        type=click.Path(dir_okay=True),
        help="Path to the environment directory to consider.")
@click.option("-h", "--hard",
        is_flag=True,
        help="Destroy the underlying resources as well.")
@click.option("-i", "--include-images", "include_images",
        is_flag=True,
        help="Remove also the docker images.")
@click.option("-l", "--local",
        is_flag=True,
        help="Do not remove images from registry.")
def destroy(env, hard, include_images, local):
    t.destroy(hard, include_images, local, env=env)


@cli.command(help="Pull Docker images (on the registry node by default).")
@click.option("-e", "--env",
        help="Path to the environment directory to consider.")
@click.option("-c", "--cached",
        is_flag=True,
        help="Pull Docker images on every OpenStack nodes.")
def bootstrap(cached, env):
    t.bootstrap(cached, env=env)


if __name__ == '__main__':
    cli()

