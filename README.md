# Madeus OpenStack Benchmark

## Presentation
This project contains the following parts:
* A program using `enoslib` to reserve and provision resources (e.g. VMs) called
  `mad-enolib.py`;
* An `execo`-based program to orchestrate the desired benchmarks through a
  parameter sweeper, named `bench-execo.py`;
* A configuration file `reservation.yaml` containing information about resources
  (used for `mad-enoslib`) and benchmark parameters (used for `bench-execo.py`);
* A submodule: This project requires a set of Madeus components, assemblies and
  assembly-deployer program to run. Such elements are available
  [here](https://gitlab.inria.fr/VeRDi-project/madeus-openstack) and included in 
  this repository as a submodule in the `madeus-openstack` directory.

## Benchmark workflow

A typical experiment using Mad is the sequence of several phases:

1. `deploy`: read the configuration file, get machines from the resource
   provider and prepare the next phase;
2. `bench`:run the benchmark according to the desired scenario defined in
   `reservation.yaml`;
3. `backup`: backup metrics gathered, logs and configuration files from the
   experiment;
4. `destroy`: release the resources;
5. `analyze`: analyze and generate graphs from the results of a backed up benchmark.

## Introduction to our tools

### Requirements
* Vagrant
* Python 3.6+
* Virtualenv

#### Vagrant specificities
	
If you plan to deploy OpenStack on Vagrant nodes, the `libvirt` plugin for
Vagrant must be installed prior any commissioning.

#### Grid5000 specificities
- Furthermore, you need to set `~/.python-grid5000.yaml` as explained
  [here](https://discovery.gitlabpages.inria.fr/enoslib/tutorials/grid5000.html#configuration)
- Finally, an appropriate SSH configuration is required to access automatically
  the distant nodes on G5K as `root` for the sake of the deployment process. By
  default, the public key sent to the distant nodes is `~/.ssh/id_rsa.pub`. Be
  sure this key exists, and that no passphrase is required before going further
  (it is otherwise recommended to create a set of key specifically for
  grid5000). Then, be sure the file `~/.ssh/config` contains the following
  parts, adapted with your own Grid 5000 username:
```
# Ease the access to the global access machine
Host g5k
    User <your_g5k_username>
    Hostname access.grid5000.fr
    ForwardAgent yes

# Ease the access to site frontends (e.g. rennes.g5k)
Host *.g5k
    User <your_g5k_username>
    ProxyCommand ssh g5k -W "`basename %h .g5k`:%p"
    ForwardAgent yes

# Ease the access to deployed nodes (e.g. paravance-42-kavlan-4.rennes.grid5000.fr)
Host *.grid5000.fr
    User root
    ProxyCommand ssh -A <your_g5k_username>@194.254.60.33 -W "$(basename %h):%p"
    ForwardAgent yes
```

### Edit the file 'reservation.yaml'
It contains a configuration definition for Vagrant and another one for Grid5000.
Adapt it to your needs.

### First steps with `mad-enoslib.py`
It is recommended to run your experiment in a virtualenv:
```
$ virtualenv -p python3 venv
$ source venv/bin/activate
(venv) $ make install_deps
(venv) $ python mad-enoslib.py --help
Usage: mad-enoslib.py [OPTIONS] COMMAND [ARGS]...

Options:
  --help  Show this message and exit.

Commands:
  backup      Backup the environment.
  bootstrap   Pull Docker images in the registry.
  deploy      Claim resources regarding the given configuration file
  destroy     Destroy the deployment and optionally the related resources.
  info        Show information of the actual deployment.
  install-os  Deploy OpenStack with kolla-ansible.

(venv) $ python mad-enoslib.py deploy --help
Usage: mad-enoslib.py deploy [OPTIONS]

  Claim resources regarding the given configuration file

Options:
  -c, --conf PATH      Path to the configuration file describing the
                       deployment.
  -e, --env PATH       Path to the environment directory to consider.
  --force              Destroy and up
  -p, --provider TEXT  Select the provider (vagrant or g5k).
  -r, --registry       Deploy an extra node containing a local registry.
  --help               Show this message and exit.
```

#### Examples
```
# Get the VM according to the configuration file `reservation.yaml`, then
# destroy them
(venv) $ python mad-enoslib.py deploy --provider vagrant
(venv) $ python mad-enoslib.py destroy --hard

# Get the nodes on Grid5000, deploy openstack using kolla-ansible, then
# destroy the resources
(venv) $ python mad-enoslib.py deploy --provider g5k
(venv) $ python mad-enoslib.py install-os
(venv) $ python mad-enoslib.py destroy --hard
```

### Run a benchmark test

```
(venv) $ ./bench-execo.py -h
usage: <program> [options] <arguments>

engine: MadBench

optional arguments:
  -h, --help            show this help message and exit
  -l LOG_LEVEL          log level (int or string). Default = inherit execo
                        logger level
  -L                    copy stdout / stderr to log files in the experiment
                        result directory. Default = False
  -R                    redirect stdout / stderr to log files in the
                        experiment result directory. Default = False
  -M                    when copying or redirecting outputs, merge stdout /
                        stderr in a single file. Default = False
  -c DIR                use experiment directory DIR
  -i ITERATIONS, --iterations ITERATIONS
                        Set the number of iterations.
  -f CONFIG_FILE, --configuration-file CONFIG_FILE
                        Set the benchmark configuration file.
  -m, --monitoring      Set EnOS monitoring stack.
  -n {0,1,2,3}, --dry-run {0,1,2,3}
                        Can be used to fire transitions in different ways: (0)
                        normal run (default); (1) with threads and fake ssh;
                        (2) with threads but no ssh; (3) without threads nor
                        ssh.
  -p PROVIDER, --provider PROVIDER
                        Set the benchmark configuration file.
  --registry {cached,local,remote}
                        Set the registry mode.
  -t TEST, --test TEST  Set the test defined in the benchmark config file.
```

#### Examples

```
# Run the 'single' benchmark scenario described in 'reservation.yaml' on Grid5000:
(venv) $ ./bench-execo.py -f reservation.yaml -t single -p g5k
```

## Reproduce our benchmarks

### Run the benchmark

The following will get the resources and bootstrap concerto-node:
```
(venv) $ python mad-enoslib.py deploy -c reservation.yaml --provider g5k --bootstrap
```

Then, run the benchmark tool by specifying which test to choose from the
configuration file:
```
(venv) $ python mad-enoslib.py bench --provider g5k --test ecotype_scenario
```

### Backup the results

```
(venv) $ python mad-enoslib.py backup --help
Usage: mad-enoslib.py backup [OPTIONS]

  Backup the results.

Options:
  -e, --env PATH         Path to the environment directory to consider.
  -b, --backup_dir PATH  Path to the result directory to consider.
  --help                 Show this message and exit. Run the 'single' benchmark scenario described in 'reservation.yaml' on Grid5000:
```

#### Example

The following stores the benchmark results in the `backups` directory. The
results contain Madeus information and OpenStack information:
1. The whole `mad` directory from `concerto-node` containing:
    * the source code,
    * the madeus environment (e.g. `current/`, `reservation.yaml`),
    * the madeus results (e.g. deployment times, stdout) `MadBench(...)/`,
    * the concerto results (i.e. transition times) as JSONs.
2. The configuration files generated for OpenStack services per nodes,
3. The log files generated by OpenStack services per nodes.

```
(venv) $ python mad-enoslib.py backup -b backups/ecotype_results
```

### Analyze the results

```
(venv) $ python mad-enoslib.py analyze -b backups/ecotype_results
```

TBD
	
